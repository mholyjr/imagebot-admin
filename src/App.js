import Routes from './routes/Routes';
import GlobalStyles from './styles/global';

function App() {
  return (
    <>
      <GlobalStyles />
      <Routes />
    </>
    
  );
}

export default App;
