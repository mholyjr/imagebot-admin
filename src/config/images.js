import LogoC from '../assets/images/logo/ib-logo-c.svg'
import LogoW from '../assets/images/logo/ib-logo-w.svg'
import LogoB from '../assets/images/logo/ib-logo-b.svg'

export const logo = {
    c: LogoC,
    w: LogoW,
    b: LogoB
}