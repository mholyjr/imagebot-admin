import React, { useState } from 'react'
import { Form, Button } from 'semantic-ui-react'
import { checkId, createProject } from '../../../api/projects'
import { jsonToFormData } from '../../../tools'
import { useHistory } from "react-router-dom"
import { formatString } from 'react-localization';
import { projectsLang } from '../../../lang/projects'

function NewProjectForm() {

    let history = useHistory();
    const [data, setData] = useState({})
    const [id_check, setIdCheck] = useState(false)
    const [loadingCheck, setLoadingCheck] = useState(false)
    const [srvError, setSrvError] = useState('')
    const [loading, setLoading] = useState(false)

    function onChange(e) {
        if(e.target.name === 'public_id') {
            setLoadingCheck(true)
            checkId(e.target.value)
            .then((res) => {
                setIdCheck(res.data.check)
                setLoadingCheck(false)
            })
            .catch((err) => {
                setIdCheck(false)
                setLoadingCheck(false)
            })
        }
        setData({...data, [e.target.name]: e.target.value})
    }

    function submit() {
        setLoading(true)
        const sendData = jsonToFormData(data)
        createProject(sendData)
        .then((res) => {
            if(res.data.code === 1) {
                history.push({
                    pathname: '/',
                    state: { message: projectsLang.formatString(projectsLang.success, data.name) }
                })
                setLoading(false)
            }
        })
        .catch((err) => {
            setSrvError(err.message)
            setLoading(false)
        })

    }

    return (
        <Form onSubmit={submit} loading={loading}>
            <Form.Input 
                name='name'
                label={projectsLang.projectName}
                type='text'
                placeholder={projectsLang.enterProjectName}
                value={data.name}
                onChange={onChange}
                required
            />
            <Form.Input 
                name='public_id'
                label={projectsLang.uniqueId}
                type='text'
                placeholder={projectsLang.enterUniqueId}
                className='mb-0'
                value={data.public_id}
                onChange={onChange}
                icon={!id_check ? 'times circle outline red' : 'check circle outline green'}
                required
                loading={loadingCheck}
            />
            {!data.public_id && <p className='small mt-1'>{projectsLang.onlyAlphaNumeric}</p> }
            {data.public_id && !id_check && <p className='small mt-1 error'>{projectsLang.alreadyInUse}</p> }
            {data.public_id && id_check &&
            <p className='small mt-1'><strong>{projectsLang.imageUrl}</strong> https://api.imagebot.app/{data.public_id.replace(/[^a-z0-9]/gi,'')}/[image_name]</p>
            }
            <Button className='success' disabled={!id_check} fluid>{projectsLang.createProject}</Button>
            {srvError && <p>{srvError}</p>}
        </Form>
    )
}

export default NewProjectForm