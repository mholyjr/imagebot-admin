import React, { useState } from 'react'
import { Button, Checkbox, Form, Icon, Message } from 'semantic-ui-react'
import { authLang } from '../../../lang/auth'
import { Loader } from '@styled-icons/boxicons-regular/Loader'
import { jsonToFormData } from '../../../tools'
import { signup } from '../../../api/auth'
import { useHistory } from "react-router-dom"

function SignupForm() {

    let history = useHistory();

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState({})
    const [errors, setError] = useState({})
    const [serverError, setServerError] = useState(false)
    const [serverSuccess, setServerSuccess] = useState(false)
    const [serverMessage, setServerMessage] = useState('')

    function onChange(e) {
        setData({...data, [e.target.name]: e.target.value})
        setError({...errors, [e.target.name]: false})
        console.log(data)
    }

    function onChangeCheckbox(e, input) {
        let checked = input.checked ? 1 : 0
        setData({...data, [input.name]: checked})
    }

    function validate() {

        var isValid = true

        if(data.password !== data.password_check) {
            isValid = false
            setError({...errors, password_check: 'Zadaná hesla se neshodují'})
        }

        if(data.password.length < 6) {
            isValid = false
            setError({...errors, password: 'Heslo musí mít minimálně 6 znaků'})
        }

        if(!validateEmail(data.email)) {
            isValid = false
            setError({...errors, email: 'Email není ve správném tvaru'})
        }

        if(!validateName(data.first_name)) {
            isValid = false
            setError({...errors, name: 'Ve jméně mohou být pouze písmena a mezery'})
        }

        if(!validateName(data.last_name)) {
            isValid = false
            setError({...errors, name: 'Ve jméně mohou být pouze písmena a mezery'})
        }

        return isValid

    }

    function validateEmail(email) {
        const re = /\S+@\S+\.\S+/;
        return re.test(String(email).toLowerCase());
    }

    function validateName(name) {
        const re = /^[ a-zA-ZÀ-ÿ\u00f1\u00d1]*$/g;
        return re.test(String(name).toLowerCase());
    }

    function submit() {
        setLoading(true)

        if(validate()) {
            var userData = jsonToFormData(data)

            signup(userData)
            .then((res) => {
                if(res.data.type === 'error') {
                    setServerError(true)
                    setServerMessage(res.data.message)
                } else if(res.data.type === 'success') {
                    setServerSuccess(true)
                    setServerMessage(res.data.message)
                    history.push({
                        pathname: '/login',
                        state: { message: authLang.success }
                    })
                }
                setLoading(false)
            })
            .catch((err) => {
                alert('Vyskytla se chyba na naší straně. Zkuste prosím registraci později.')
            })
        } else {
            setLoading(false)
        }
    }

    return (
        <>
            <Form onSubmit={() => submit()} loading={loading}>
                <Form.Input 
                    type='text'
                    name='first_name'
                    id="first_name"
                    placeholder={authLang.enterFirstName}
                    label={authLang.firstName}
                    onChange={onChange}
                    error={errors.name}
                    required
                />

                <Form.Input 
                    type='text'
                    name='last_name'
                    id="last_name"
                    placeholder={authLang.enterLastName}
                    label={authLang.lastName}
                    onChange={onChange}
                    error={errors.name}
                    required
                />
                
                <Form.Input 
                    type='email'
                    name='email'
                    id='signup_email'
                    placeholder={authLang.enterEmail}
                    label='Email'
                    required
                    onChange={onChange}
                    error={errors.email}
                />
                <Form.Input 
                    name='password'
                    type='password'
                    id='signup_password'
                    placeholder={authLang.enterPassword}
                    label={authLang.password}
                    required
                    onChange={onChange}
                    error={errors.password}
                />
                <Form.Input 
                    name='password_check'
                    type='password'
                    id='signup_password_check'
                    placeholder={authLang.enterPasswordCheck}
                    label={authLang.passwordCheck}
                    required
                    onChange={onChange}
                    error={errors.password_check}
                />
                <Form.Field>
                    <Checkbox 
                        label={<label>Souhlasím s Všeobecnými obchodními podmínkami</label>}
                        name='terms'
                        onChange={onChangeCheckbox}
                    />
                </Form.Field>
                <Form.Field>
                    <Checkbox
                        label={<label>Souhlasím se Zásadami zpracování osobních údajů</label>}
                        name='gdpr'
                        onChange={onChangeCheckbox}
                    />
                </Form.Field>
                <Button className='success' fluid disabled={data.gdpr !== 1 || data.terms !== 1}>{authLang.signup}</Button>
            </Form>
        </>
    )
}

export default SignupForm