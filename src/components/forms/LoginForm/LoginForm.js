import React, { useState } from 'react'
import { Button, Form, Message } from 'semantic-ui-react'
import { signin } from '../../../api/auth'
import { authLang } from '../../../lang/auth'
import { jsonToFormData } from '../../../tools'
import { useDispatch } from "react-redux";
import { saveUser } from '../../../actions/userActions'
import { useHistory } from "react-router-dom"

function LoginForm({ message }) {

    const dispatch = useDispatch();
    let history = useHistory();

    const [loading, setLoading] = useState(false)
    const [data, setData] = useState({})
    const [errors, setError] = useState({})
    const [serverError, setServerError] = useState(false)
    const [serverSuccess, setServerSuccess] = useState(false)
    const [serverMessage, setServerMessage] = useState('')

    function onChange(e) {
        setData({...data, [e.target.name]: e.target.value})
        setError({...errors, [e.target.name]: false})
    }

    function submit() {
        setLoading(true)

        var userData = jsonToFormData(data)

        signin(userData)
        .then((res) => {
            if(res.data.type === 'error') {
                setServerError(true)
                setServerMessage(res.data.message)
            } else if(res.data.type === 'success') {
                setServerSuccess(true)
                setServerMessage(res.data.message)
                dispatch(saveUser(res.data.user))
                history.push('/');
            }
            setLoading(false)
        })
        .catch((err) => {
            alert('Vyskytla se chyba na naší straně. Zkuste prosím registraci později.')
        })
    }

    return (
        <>
            <Form onSubmit={() => submit()}>
                {message &&
                <Message content={message} positive />
                }
                <Form.Input 
                    type='email'
                    name='email'
                    id='signup_email'
                    placeholder={authLang.enterEmail}
                    label='Email'
                    required
                    onChange={onChange}
                    error={errors.email}
                />
                <Form.Input 
                    name='password'
                    type='password'
                    id='signup_password'
                    placeholder={authLang.enterPassword}
                    label={authLang.password}
                    required
                    onChange={onChange}
                    error={errors.password}
                />
                <Button className='success' fluid>{authLang.login}</Button>
            </Form>
        </>
    )
}

export default LoginForm