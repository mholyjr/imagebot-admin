import React from 'react'
import { Col, Row } from 'reactstrap'
import TrafficGraph from '../../containers/TrafficGraph/TrafficGraph'

function DashboardView() {

    return (
        <>
            <Row>
                <Col>
                    <TrafficGraph />
                </Col>
            </Row>
        </>
    )
}

export default DashboardView