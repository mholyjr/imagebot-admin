import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormHeader, FormWrapper, PageWrapper } from '../../../styles/auth'
import LoginForm from '../../forms/LoginForm/LoginForm'
import { PersonPlus } from '@styled-icons/bootstrap'
import { authLang } from '../../../lang/auth'
import SignupForm from '../../forms/SignupForm/SignupForm'
import { Link } from 'react-router-dom'

function SignupView() {

    return (
        <PageWrapper>
            <div>
                <FormWrapper>
                    <FormHeader>
                        <Row className='align-items-center'>
                            <Col xs={2}>
                                <PersonPlus />
                            </Col>
                            <Col xs={10}>
                                <h1>{authLang.signupTo}</h1>
                            </Col>
                        </Row>
                    </FormHeader>
                    <SignupForm />
                </FormWrapper>
                <Link to='/login'>Already have an account? Login here!</Link>
            </div>
        </PageWrapper>
    )
}

export default SignupView