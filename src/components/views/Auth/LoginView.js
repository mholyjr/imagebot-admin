import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormHeader, FormWrapper, PageWrapper } from '../../../styles/auth'
import LoginForm from '../../forms/LoginForm/LoginForm'
import { ShieldLock } from '@styled-icons/bootstrap'
import { authLang } from '../../../lang/auth'
import { Link } from 'react-router-dom'

function LoginView(props) {
    console.log(props.location.state)
    return (
        <PageWrapper>
            <div>
            <FormWrapper>
                <FormHeader>
                    <Row className='align-items-center'>
                        <Col xs={2}>
                            <ShieldLock />
                        </Col>
                        <Col xs={10}>
                            <h1>{authLang.loginTo}</h1>
                        </Col>
                    </Row>
                </FormHeader>
                <LoginForm message={props.location.state ? props.location.state.message : undefined} />
            </FormWrapper>
            <Link to='/signup'>Don't have an account? Sign up here!</Link>
            </div>
        </PageWrapper>
    )
}

export default LoginView