import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormHeader, FormWrapper, PageWrapper } from '../../../styles/auth'
import { PlusCircle } from '@styled-icons/bootstrap'
import { projectsLang } from '../../../lang/projects'
import NewProjectForm from '../../forms/NewProjectForm/NewProjectForm'
import { useDispatch, useSelector } from "react-redux";
import { checkUser } from '../../../api/auth';
import { saveUser } from '../../../actions/userActions';
import LoaderContainer from '../../containers/Loader/LoaderContainer'
import { useHistory } from "react-router-dom"
import { globalLang } from '../../../lang/global'

function NewProject() {

    const dispatch = useDispatch();
    let history = useHistory();
    const [loadingCheck, setLoading] = useState(true)

    useEffect(() => {
        checkUser()
        .then((res) => {
            if(res.data.type === 'success') {
                dispatch(saveUser(res.data.user))
                setLoading(false)
            } else {
                history.push({
                    pathname: '/login',
                    state: { message: globalLang.needToLogin }
                })
            }
        })
    }, [])

    const logged = useSelector(state => state.user.logged);
    const loading = useSelector(state => state.user.loading);
    const projects = useSelector(state => state.user.user.projects)

    if(!loadingCheck) {
        return (
            <PageWrapper>
                <div>
                <FormWrapper>
                    <FormHeader>
                        <Row className='align-items-center'>
                            <Col xs={2}>
                                <PlusCircle />
                            </Col>
                            <Col xs={10}>
                                <h1>{projectsLang.newProject}</h1>
                            </Col>
                        </Row>
                    </FormHeader>
                    <NewProjectForm />
                </FormWrapper>
                </div>
            </PageWrapper>
        )
    } else {
        return (
            <LoaderContainer />
        )
    }
    
}

export default NewProject