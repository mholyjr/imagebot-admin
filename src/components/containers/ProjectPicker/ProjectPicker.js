import React from 'react'
import { Dropdown } from 'semantic-ui-react'
import { useDispatch, useSelector } from "react-redux";
import { selectProject } from '../../../api/user';
import { saveUser } from '../../../actions/userActions';

function ProjectPicker() {

    const dispatch = useDispatch();

    const logged = useSelector(state => state.user.logged);
    const loading = useSelector(state => state.user.loading);
    const projects = useSelector(state => state.user.user.projects);
    const selected = useSelector(state => state.user.user.selected);
    const user = useSelector(state => state.user.user);

    function _selectProject(e, data) {
        selectProject(data.value)
        .then((res) => {
            if(res.data.code === 1) {
                const oldUser = user
                const newUser = {...user, selected: res.data.user.selected}
                dispatch(saveUser(newUser))
            }
        })
    }

    return (
        <>
        <Dropdown 
            placeholder='Select Country'
            fluid
            search
            selection
            options={logged && !loading ? projects : []}
            value={logged && !loading ? selected : null}
            loading={loading}
            onChange={_selectProject}
        />
        </>
    )
}

export default ProjectPicker