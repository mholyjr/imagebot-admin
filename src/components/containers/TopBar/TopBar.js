import React from 'react'
import { Button } from 'semantic-ui-react'
import { logout } from '../../../api/auth'
import { NavbarWrapper } from '../../../styles/navbar'
import UserInfo from './UserInfo'
import { useDispatch } from "react-redux";
import { purgeUser } from '../../../actions/userActions'

function TopBar() {

    const dispatch = useDispatch();

    function _logout() {
        logout()
        .then((res) => {
            if(res.data.type === 'success') {
                dispatch(purgeUser())
            }
        })
    }

    return (
        <NavbarWrapper>
            <UserInfo />
            <Button onClick={_logout}>Sign out</Button>
        </NavbarWrapper>
    )
}

export default TopBar