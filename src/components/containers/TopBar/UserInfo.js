import React from 'react'
import { useSelector } from 'react-redux';
import { PersonCircle } from 'styled-icons/bootstrap';
import { UserInfoWrapper } from '../../../styles/navbar';

function UserInfo() {

    const user = useSelector(state => state.user.user);

    return (
        <UserInfoWrapper>
            <PersonCircle color='#fff' size={22} />
            <span>{user.first_name} {user.last_name}</span>
        </UserInfoWrapper>
    )
}

export default UserInfo