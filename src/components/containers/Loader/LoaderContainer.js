import React from 'react'
import { PageWrapper } from '../../../styles/auth'
import { Loader } from '@styled-icons/boxicons-regular/Loader'

function LoaderContainer() {

    return (
        <PageWrapper pos='fixed'>
            <Loader size={40} className='spin' color='#fff' />
        </PageWrapper>
    )
}

export default LoaderContainer