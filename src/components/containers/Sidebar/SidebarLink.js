import React from 'react'
import { CustomLink } from '../../../styles/sidebar'

function SidebarLink({ item }) {

    return (
        <CustomLink to={item.link} exact={item.exact}>{item.icon} {item.title}</CustomLink>
    )
}

export default SidebarLink