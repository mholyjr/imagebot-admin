import React from 'react'
import { Link } from 'react-router-dom'
import { Col, Row } from 'reactstrap'
import { Button } from 'semantic-ui-react'
import { Gear, Speedometer2, CardImage, Plus } from 'styled-icons/bootstrap'
import { logo } from '../../../config/images'
import { navLang } from '../../../lang/nav'
import { Logo } from '../../../styles/logo'
import { SidebarWrapper, SidebarContent, SidebarLogo, SidebarProjectPicker } from '../../../styles/sidebar'
import ProjectPicker from '../ProjectPicker/ProjectPicker'
import SidebarLink from './SidebarLink'

const links = [
    {
        title: navLang.dashboard,
        link: '/',
        icon: <Speedometer2 size={22} />,
        exact: true
    },
    {
        title: navLang.myFiles,
        link: '/files',
        icon: <CardImage size={22} />,
        exact: false
    },
    {
        title: navLang.settings,
        link: '/settings',
        icon: <Gear size={22} />,
        exact: false
    }
]

function Sidebar({show, toggle}) {

    return (
        <SidebarWrapper show={show} className='shadow-lg'>
            <SidebarLogo>
                <Logo src={logo.c} maxWidth='60%' />
            </SidebarLogo>
            <SidebarProjectPicker>
                <Row className='mr-0'>
                    <Col xs={10}>
                        <ProjectPicker />
                    </Col>
                    <Col xs={2} className='p-0 d-flex justify-content-end new_project_btn'>
                        <Link to='/new-project'><Button icon><Plus size={32} /></Button></Link>
                    </Col>
                </Row>
            </SidebarProjectPicker>
            <SidebarContent>
                {
                    links.map((item) => {
                        return <SidebarLink item={item} />
                    })
                }
            </SidebarContent>
        </SidebarWrapper>
    )
}

export default Sidebar