import React, { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import { getGraph } from '../../../api/stats'
import { useSelector } from "react-redux";
import { Card, CardHeader, CardContent } from '../../../styles/cards';
import { graphLang } from '../../../lang/graph'
import { Dropdown } from 'semantic-ui-react'

const typeValues = [
    {key: '0', value: 'today', text: graphLang.today},
    {key: '1', value: 'this-month', text: graphLang.thisMonth},
    {key: '2', value: 'this-year', text: graphLang.thisYear}
]

function TrafficGraph() {

    const user = useSelector(state => state.user.user);
    const [labels, setLabels] = useState([])
    const [values, setValues] = useState([])
    const [bg, setBg] = useState([])
    const [type, setType] = useState('this-month')

    useEffect(() => {
        getData()
    }, [user, type])

    function getData() {
        getGraph(type)
        .then((res) => {
            if(res.data.code === 1) {
                setLabels(res.data.data.labels)
                setValues(res.data.data.data)
                setBg(res.data.data.bg)
            }
        })
    }

    function prepareData() {
        const data = {
            labels: labels,
            datasets: [{
                label: 'Traffic in MB',
                data: values,
                backgroundColor: bg,
                borderColor: bg,
                borderWidth: 1
            }]
        }

        return data
    }

    function changeState(e, data) {
        setType(data.value)
    }

    return (
        <Card bg='#172b4d'>
            <CardHeader theme='dark'>
                <h5>{graphLang.trafficHeadline}</h5>
                <Dropdown 
                    placeholder='Select'
                    selection
                    value={type}
                    options={typeValues}
                    onChange={changeState}
                />
            </CardHeader>
            <CardContent height='500px' theme='dark'>
                <Bar 
                    data={prepareData}
                    width={100}
                    height={500}
                    options={{ 
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                            labels: {
                                fontColor: '#6E7F94',
                                fontSize: 18
                            }
                        },
                        scales: {
                            xAxes: [{ 
                                gridLines: {
                                    display: false,
                                },
                                ticks: {
                                  fontColor: "#6E7F94",
                                  fontStyle: 'bold'
                                },
                            }],
                            yAxes: [{
                                ticks: {
                                    fontColor: "#6E7F94",
                                    fontStyle: 'bold',
                                    padding: 10,
                                    callback: function(value, index, values) {
                                        return value + ' MB';
                                    }
                                },
                                gridLines: {
                                    display: true,
                                    color: '#6E7F94',
                                    drawBorder: false,
                                    zeroLineColor: '#6E7F94',
                                    borderDash: [2, 2]
                                },
                            }],
                        }
                    }}
                />
            </CardContent>
            
        </Card>
        
    )
}

export default TrafficGraph