import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
    body {
        --primary: #167BD9;
        --accent: #F27E61;
        --success: #39C47A;
        --error: #D9352B;
        --default_radius: 9px;
        --small_radius: 6px;
        --border: 1px solid #ececec;
        --border_dark: 1px solid #1d355f;
        --success_hover: #268552;
        --bg_secondary: #FBFBFB;
        --shadow: 0 .5rem 1rem rgba(0,0,0,.15);
        background: var(--bg_secondary);
    }

    label {
        font-weight: 700;
        font-size: 14px;
    }

    .btn-primary {
        background-color: var(--primary);
        border-color: var(--primary);
    }

    .btn-success, .ui.fluid.button.success {
        background-color: var(--success);
        border-color: var(--success);
        color: #fff;
    }

    .btn-success:hover, .ui.fluid.button.success:hover {
        background-color: var(--success_hover);
        border-color: var(--success_hover);
    }

    .ui.attached.positive.message, .ui.positive.message {
        border: 1px solid var(--success);
        box-shadow: none;
    }

    .ui.positive.message .content p {
        color: var(--success);
        font-weight: 700;
    }

    .spin {
        animation: spinner 5s ease-out infinite;
    }

    @keyframes spinner {
        0% {
            transform: rotateZ(0)
        }
        100% {
            transform: rotate(360deg);
        }
    }

    .tw {
        color: #fff;
    }

    .shadow-lg {
        box-shadow: 0 1rem 3rem rgba(0,0,0,.07)!important;
    }

    div#root::before {
        content: '';
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        height: 350px;
        background: var(--primary);
        z-index: -1;
    }

    p.error,
    span.error {
        color: var(--error);
    }

    .ui.icon.button {
        padding: 0;
    }

    .new_project_btn a {
        width: 100%;
        height: 100%;
    }

    .new_project_btn .ui.button {
        width: 100%;
        height: 100%;
        background: var(--success);
        color: #fff;
    }

    .new_project_btn .ui.button:hover {
        background: var(--success_hover);
    }
`

export default GlobalStyles