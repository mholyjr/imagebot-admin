import styled from 'styled-components'

export const NavbarWrapper = styled.div`
    padding: 2rem 0;
    display: flex;
    justify-content: flex-end;
    align-items: center;
`

export const UserInfoWrapper = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding-right: 20px;

    span {
        color: #fff;
        font-weight: 700;
    }

    svg {
        margin-right: 10px;
    }
`