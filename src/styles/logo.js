import styled from 'styled-components'

export const Logo = styled.img`
    max-width: ${props => props.maxWidth};
`