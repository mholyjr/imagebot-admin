import styled from 'styled-components'

export const Card = styled.div`
    width: 100%;
    box-shadow: var(--shadow);
    border-radius: var(--default_radius);
    background: ${props => props.bg ? props.bg : '#fff'};
`

export const CardHeader = styled.div`
    width: 100%;
    border-bottom: var(--border);
    padding: 1.25rem 1.5rem;
    display: flex;
    justify-content: space-between;
    align-items: center;

    h5 {
        font-size: 1.2rem;
        font-weight: 700;
        margin: 0;
    }

    ${props => props.theme === 'dark' && 'h5 { color: #fff; }'}
    ${props => props.theme === 'dark' && 'border-bottom: var(--border_dark);'}
`

export const CardContent = styled.div`
    ${props => props.height && 'height: ' + props.height + ';'}
    padding: 1.5rem;

    canvas {
        width: 100%;
    }
`