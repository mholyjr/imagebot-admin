import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { Logo } from './logo'

export const SidebarWrapper = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    bottom: 0;
    width: ${props => props.show ? '20%' : 0};
    background: #fff;
    transition: all .3s ease-out;
    overflow-x: hidden;
    overflow-y: auto;
`

export const SidebarContent = styled.div`
    padding: 1.5rem 1.5rem;
`

export const SidebarLogo = styled.div`
    padding: 3rem 1.5rem;
    border-bottom: var(--border);

    & > ${Logo} {
        margin: 0 auto;
        display: block;
    }
`

export const SidebarProjectPicker = styled.div`
    padding: 1.5rem 1.5rem;
    border-bottom: var(--border);
    background: var(--bg_secondary);
`

export const CustomLink = styled(NavLink)`
    padding: 1.5rem;
    display: flex;
    align-items: center;
    color: var(--primary);
    transition: all .3s ease-out;
    border-radius: var(--small_radius);
    margin-bottom: 10px;
    font-weight: 700;
    font-size: 16px;

    :hover, &.active {
        color: #fff;
        background: var(--primary);
        box-shadow: var(--shadow);
    }

    svg {
        margin-right: 15px;
    }
`