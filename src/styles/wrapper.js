import styled from 'styled-components'

export const Wrapper = styled.div`
    width: ${props => props.show ? '80%' : '100%'};
    margin-left: ${props => props.show ? '20%' : 0};
    transition: all .3s ease-out;
    padding: 0 3rem 3rem;
`