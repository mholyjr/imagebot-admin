import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const PageWrapper = styled.div`
    width: 100%;
    height: 100vh;
    background: ${props => props.bg === 'transparent' ? 'transparent' : 'var(--primary)'};
    display: flex;
    justify-content: center;
    align-items: center;

    & > div {
        width: 100%;
        max-width: 400px;
    }

    & > div > a {
        color: #fff;
        text-align: center;
        display: block;
        margin-top: 1rem;
    }

    ${props => props.pos === 'fixed' && 'position: fixed; top: 0; left: 0; right: 0;'}
`

export const FormHeader = styled.div`
    width: 100%;
    padding: 1rem 0 2rem;
    border-bottom: var(--border);
    margin-bottom: 2rem;

    h1 {
        font-size: 22px;
        font-weight: 700;
        margin: 0;
    }

    .col-2 {
        border-right: var(--border);
    }
`

export const FormWrapper = styled.div`
    max-width: 400px;
    padding: 1.5rem 1.5rem 3.5rem;
    width: 100%;
    background: #fff;
    border-radius: var(--default_radius);
`