import axios from 'axios';
import { api_url, token } from '../config/api';

export function signup(data) {
    return axios({
        method: 'post',
        url: api_url + 'api/users/signup',
        data: data,
        headers: { Token: token },
        withCredentials: true
    })
}

export function signin(data) {
    return axios({
        method: 'post',
        url: api_url + 'api/users/signin',
        data: data,
        headers: { Token: token },
        withCredentials: true
    })
}

export function checkUser() {
    return axios({
        method: 'get',
        url: api_url + 'api/users/check/',
        headers: { Token: token },
        withCredentials: true
    })
}

export function logout() {
    return axios({
        method: 'get',
        url: api_url + 'api/users/logout/',
        headers: { Token: token },
        withCredentials: true
    })
}