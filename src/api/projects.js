import axios from 'axios';
import { api_url, token } from '../config/api';

export function createProject(data) {
    return axios({
        method: 'post',
        url: api_url + 'api/projects/create',
        data: data,
        headers: { Token: token },
        withCredentials: true
    })
}

export function checkId(id) {
    return axios({
        method: 'get',
        url: api_url + 'api/projects/check-id/' + id,
        headers: { Token: token },
        withCredentials: true
    })
}