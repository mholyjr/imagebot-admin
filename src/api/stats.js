import axios from 'axios';
import { api_url, token } from '../config/api';

export function getGraph(type) {
    return axios({
        method: 'get',
        url: api_url + 'api/stats/graph/' + type,
        headers: { Token: token },
        withCredentials: true
    })
}