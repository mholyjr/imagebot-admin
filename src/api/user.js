import axios from 'axios';
import { api_url, token } from '../config/api';

export function selectProject(id) {
    return axios({
        method: 'get',
        url: api_url + 'api/users/select/' + id,
        headers: { Token: token },
        withCredentials: true
    })
}