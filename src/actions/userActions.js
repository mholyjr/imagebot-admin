import * as types from './types'

export function saveUser(data) {
    return (dispatch, getState) => {
        dispatch({type: types.USER_LOGGED_IN, user: data})
    }
}

export function purgeUser() {
    return (dispatch, getState) => {
        dispatch({type: types.USER_LOGGING_OUT})
    }
}