import React, { useContext, useState } from 'react'
import { useEffect } from 'react';
import { Redirect, Route } from 'react-router'
import { useDispatch, useSelector } from "react-redux";
import { checkUser } from '../api/auth';
import { saveUser } from '../actions/userActions';
import LoaderContainer from '../components/containers/Loader/LoaderContainer';

function PrivateRoute ({ component: Component, ...rest }) {

    const dispatch = useDispatch();
    const [loadingCheck, setLoading] = useState(true)

    useEffect(() => {
        checkUser()
        .then((res) => {
            if(res.data.type === 'success') {
                dispatch(saveUser(res.data.user))
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const logged = useSelector(state => state.user.logged);
    const loading = useSelector(state => state.user.loading);
    const projects = useSelector(state => state.user.user.projects)
    
    if(!loadingCheck) {
        return (
            <Route {...rest} render={() => {return !logged ? <Redirect to='/login' /> : (projects.length !== 0 ? <Component /> : <Redirect to='/new-project' />) }} />
        )
    } else {
        return (
            <LoaderContainer />
        )
    }
}

export default PrivateRoute