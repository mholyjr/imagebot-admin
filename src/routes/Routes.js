import React, { useState } from 'react'
import { Route, Switch, withRouter, useRouteMatch } from 'react-router'
import LoginView from '../components/views/Auth/LoginView'
import Sidebar from '../components/containers/Sidebar/Sidebar'
import { Wrapper } from '../styles/wrapper'
import SignupView from '../components/views/Auth/SignupView'
import PrivateRoute from './PrivateRoute'
import DashboardView from '../components/views/Dashboard/DashboardView'
import TopBar from '../components/containers/TopBar/TopBar'
import NewProject from '../components/views/Projects/NewProject'

function Routes() {

    const [showSidebar, setShowSidebar] = useState(true)
    const loginRoute = useRouteMatch("/login");
    const signupRoute = useRouteMatch("/signup");
    const newprojectRoute = useRouteMatch("/new-project");

    function toggleSidebar() { setShowSidebar(!showSidebar) }

    return (
        <>
        {!loginRoute && !signupRoute && !newprojectRoute &&
            <Sidebar show={showSidebar} toggle={toggleSidebar} />
        }
            <Switch>
                <Route component={withRouter(LoginView)} exact path='/login' />
                <Route component={withRouter(SignupView)} exact path='/signup' />
                <Route component={NewProject} exact path='/new-project' />
                <Wrapper show={showSidebar}>
                    <TopBar />
                    <PrivateRoute component={DashboardView} exact path='/' />
                </Wrapper>
            </Switch>
        </>
    )
}

export default Routes