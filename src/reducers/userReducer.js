import * as types from '../actions/types'

let userState = { user: {}, loading: true, logged: false }

const user = (state = userState, action) => {
    switch (action.type) {
        case types.USER_LOGGING_IN:
            return { 
                ...state,
                loading: true,
                logged: false
            }

        case types.USER_LOGGED_IN:
            return { 
                user: action.user,
                loading: false,
                logged: true
            }

        case types.USER_LOGGING_OUT:
            return userState

        default: 
            return state
    }
}

export default user