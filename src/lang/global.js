import LocalizedStrings from 'react-localization';
 
export let globalLang = new LocalizedStrings({
    en:{
        needToLogin: 'You need to login to view this page'
    }
});