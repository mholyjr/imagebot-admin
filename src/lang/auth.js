import LocalizedStrings from 'react-localization';
 
export let authLang = new LocalizedStrings({
    en:{
        loginTo: 'Login to Imagebot',
        login: 'Login',
        enterEmail: 'Enter your email',
        password: 'Password',
        enterPassword: 'Enter your password',
        passwordCheck: 'Password again',
        enterPasswordCheck: 'Enter your password again',
        firstName: 'First name',
        enterFirstName: 'Enter your first name',
        lastName: 'Last name',
        enterLastName: 'Enter your last name',
        signup: 'Sign up',
        signupTo: 'Sign up to Imagebot',
        success: 'Thank you! Now you can log in and set up first project.'
    }
});