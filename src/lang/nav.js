import LocalizedStrings from 'react-localization';

export let navLang = new LocalizedStrings({
    en: {
        dashboard: 'Dashboard',
        myFiles: 'My files',
        settings: 'Settings'
    }
});