import LocalizedStrings from 'react-localization';

export let graphLang = new LocalizedStrings({
    en: {
        trafficHeadline: 'Traffic overview',
        thisMonth: 'This month',
        thisYear: 'This year',
        today: 'Today'
    }
});