import LocalizedStrings from 'react-localization';

export let projectsLang = new LocalizedStrings({
    en: {
        newProject: 'New project',
        createProject: 'Create project',
        success: 'Project {0} was successfully created',
        projectName: 'Project name',
        enterProjectName: 'Enter project name',
        uniqueId: 'Unique ID',
        enterUniqueId: 'Enter unique ID',
        alreadyInUse: 'This ID is already in use',
        onlyAlphaNumeric: 'Use only alpha-numeric characters',
        imageUrl: 'Images URL:'
    }
});